/**
 * main.js
 *
 * Author: ideenfrische
 *
 */

/*******
ICONS
*******/
var Icons = $(window).iconloader({
 icons: 'files/template/dist/icons/',
 custom: {}
});
// after changing css classes you will need to rebuild icons
// Icons.rebuild();

/*******
COOKIES
*******/
// Cookie({
//   layout: "popup",
//   type: "steps",
//   lockNavigation: true,
//   info_target: false,
//   auto_remove: true,
//   cookies: {
//     necessary: [],
//     preferences: [],
//     statistics: [],
//     marketing: []
//   }
// });

$(function() {

});
