'use strict';

var pngquant = require('imagemin-pngquant'),
    glob = require('glob'),
    gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    beeper = require('beeper'),
    colors = require('ansi-colors');

var dirs = {
  css: 'files/template/css',
  js: 'files/template/js',
  favicon: 'files/template/favicon',
  icons: 'files/template/icons',
  dist: 'files/template/dist'
}

var jsdirs = [
  dirs.js + '/*.js',
  '!' + dirs.js + '/build.js'
]

var autoprefixerOptions = {};


const generateDate = ()=>{
  const date = new Date();

  let day = date.getDate()>=10?date.getDate():'0'+date.getDate();
  let month = date.getMonth() +1;
  month = month>=10?month:'0'+month;

  let hour = date.getHours()>=10?date.getHours():'0'+date.getHours();
  let minute = date.getMinutes()>=10?date.getMinutes():'0'+date.getMinutes();

  return day+'.'+month+'.'+date.getFullYear()+' '+hour+':'+minute;
}

const getHeader = (file,ext=[])=>{
  let header = "/**\n";
  header += " * "+file+"\n\n";
  header += " * ©ideenfrische\n";
  header += " * generated "+generateDate()+"\n";
  if(ext.length){
    header += " *\n";
    header += " * extends:\n";
    ext.forEach(v=>{
      header += " * - "+v+"\n";
    })
  }
  header += " */\n\n";
  return header;
}

/**
 * error Handler function
 * See https://github.com/mikaelbr/gulp-notify/issues/81#issuecomment-100422179
 */

var reportError = (error)=>{

  var lineNumber = (error.line ? error.line : false);
  var file = (error.message ? error.message.split('\n', 1)[0] : false);

  plugins.notify({
    title: 'Task Failed [' + error.plugin + ']',
    message: (file ? file + ', ' : '') + (lineNumber ? 'Line: ' + lineNumber + ', ' : '') + 'See console for more info...',
  }).write(error);

  beeper();


  // Pretty error reporting
  var report = '';
  var chalk = colors.red;

  report += chalk('TASK:') + ' [' + error.plugin + ']\n';
  if (lineNumber) { report += chalk('LINE:') + ' ' + lineNumber + '\n'; }
  report += chalk('PROB:') + '\n' + error.message;

  console.error(report);
}

gulp.task('sass:dev', async () => {
  gulp.src(dirs.css + '/*.scss')
    .pipe(plugins.plumber({
      errorHandler: reportError
    }))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass().on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer(autoprefixerOptions))
    .pipe(plugins.sourcemaps.write())
    .pipe(gulp.dest(dirs.dist+'/css'))
    .pipe(plugins.livereload())
    .on('error', reportError);
});

gulp.task('sass', async () => {
  gulp.src(dirs.css + '/*.scss')
    .pipe(plugins.sass({outputStyle: 'compressed'}).on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer(autoprefixerOptions))
    .pipe(plugins.header(getHeader(dirs.css,['normalize.css'])))
    .pipe(gulp.dest(dirs.dist+'/css'))
    .pipe(plugins.livereload());
});

gulp.task('imagemin', async () => {
  gulp.src(dirs.favicon + '/**/*')
    .pipe(plugins.imagemin([
      plugins.imagemin.mozjpeg({quality: 100, progressive: true}),
      plugins.imagemin.optipng({optimizationLevel: 5}),
      plugins.imagemin.svgo({
        plugins: [
          {removeViewBox: false},
          {cleanupIDs: false}
        ]
      })
    ]))
    .pipe(gulp.dest(dirs.dist+'/favicon'));
  gulp.src(dirs.icons + '/*.svg')
    .pipe(plugins.imagemin([
      plugins.imagemin.svgo({
        plugins: [
          {removeViewBox: false},
          {cleanupIDs: false}
        ]
      })
    ]))
    .pipe(gulp.dest(dirs.dist+'/icons'));
});

gulp.task('js', async ()=>{
  var jsdirsClone = jsdirs.slice(0);
  jsdirsClone.unshift('node_modules/iconloader/loader.jquery.js');
  jsdirsClone.unshift('node_modules/formjs/formjs.jquery.js');
  jsdirsClone.unshift('node_modules/jquery/dist/jquery.js');
  //jsdirsClone.unshift('node_modules/cookie/dist/cookie.js');
  jsdirsClone.unshift(dirs.js + '/modernizr.js');

  gulp.src(jsdirsClone, {allowEmpty: true})
    .pipe(plugins.plumber({
      errorHandler: reportError
    }))
    .pipe(plugins.babel({
      presets: ['@babel/env']
    }))
    .pipe(plugins.uglify())
    .pipe(plugins.concat('build.js'))
    .pipe(plugins.header(getHeader('build.js',jsdirsClone)))
    .pipe(gulp.dest(dirs.dist+'/js/'))
    .pipe(plugins.livereload())
    .on('error', reportError);
});

gulp.task('modernizr', async ()=>{
  let jsdirsClone = [...jsdirs];
  jsdirsClone.push('!'+dirs.js+'/modernizr.js');
  console.log(jsdirsClone);
  return gulp.src(jsdirsClone)
    .pipe(plugins.modernizr('modernizr.js',{
      tests: []
    }))
    .pipe(plugins.uglify())
    .pipe(gulp.dest(dirs.js+'/'));
});

gulp.task('watch', ()=>{
  plugins.livereload.listen();
  gulp.watch(dirs.css + '/*.scss', gulp.series(['sass:dev']));
  gulp.watch(jsdirs, gulp.series(['js']));
});

gulp.task('default',gulp.series('sass', 'modernizr', 'js', 'imagemin',(d)=>{d()}));
gulp.task('dev',gulp.series('default', 'watch',(d)=>{d()}));
